const request = require("../utils/axios");
const getcat = (client_id, access_token, category_id) =>
  request(
    { 
    method: "get",
    url: `/v1/category/`+category_id,
    headers: {"client-id": client_id,
    "Authorization": access_token}, 
  });
  module.exports = { getcat};
