const request = require("../utils/axios");
const delcat = (data, client_id, access_token) =>
  request(
    { 
    method: "delete",
    url: `/v1/category`,
    data,
    headers: {"client-id": client_id,
    "Authorization": access_token}, 
  });
  module.exports = { delcat};