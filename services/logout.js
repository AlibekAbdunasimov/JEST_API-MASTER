const request = require("../utils/axios");
const logout = (data, client_id) =>
  request(
    { 
    method: "post",
    url: `/v1/auth/logout`,
    data, 
    headers: {"client-id" : client_id},
  });

module.exports = {
  logout,
};