const request = require("../utils/axios");
const createcat = (data, client_id, access_token) =>
  request(
    { 
    method: "post",
    url: `/v1/category`,
    data,
    headers: {"client-id": client_id,
    "Authorization": access_token}, 
  });
  module.exports = { createcat};