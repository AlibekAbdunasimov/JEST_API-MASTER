const request = require("../utils/axios");

const loginShipper = (data, client_id) =>
  request(
    { 
    method: "post",
    url: `/v1/auth/login/system-user`,
    data,
    headers: {"client-id": client_id}, 
  }
    // console.log("-----", data)
  );

module.exports = {
  loginShipper,
};
