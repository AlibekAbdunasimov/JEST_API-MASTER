const request = require("../utils/axios");
const refreshtoken = (data, client_id) =>
  request(
    { 
    method: "post",
    url: `/v1/auth/token/refresh`,
    data, 
    headers: {"client-id" : client_id},
  });

module.exports = {
  refreshtoken,
};