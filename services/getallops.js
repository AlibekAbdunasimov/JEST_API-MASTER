const request = require("../utils/axios");
const getallops = (client_id, access_token, category_id) =>
  request(
    { 
    method: "get",
    url: `/v1/operator`,
    headers: {"client-id": client_id,
    "Authorization": access_token},
    query: {category_id: category_id,
    limit: 3,
    page: 3 } 
  });
  module.exports = { getallops};