const request = require("../utils/axios");
const getallcats = (client_id, access_token) =>
  request(
    { 
    method: "get",
    url: `/v1/category`,
    headers: {"client-id": client_id,
    "Authorization": access_token}, 
  });
module.exports = {
  getallcats,
};