const request = require("../utils/axios");
const createopr = (data, client_id, access_token,) =>
  request(
    { 
    method: "post",
    url: `/v1/operator`,
    data,
    headers: {"client-id": client_id,
    "Authorization": access_token},
    } 
  );
  module.exports = { createopr};