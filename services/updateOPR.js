const request = require("../utils/axios");
const updateopr = (operator_id,client_id, access_token, data) =>
  request(
    { 
    method: "put",
    url: `/v1/operator/`+ operator_id,
    headers: {"client-id": client_id,
    "Authorization": access_token},
    data
  });
  module.exports = { updateopr};