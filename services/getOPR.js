const request = require("../utils/axios");
const getopr = (operator_id,client_id, access_token) =>
  request(
    { 
    method: "get",
    url: `/v1/operator/`+ operator_id,
    headers: {"client-id": client_id,
    "Authorization": access_token},
  });
  module.exports = { getopr};