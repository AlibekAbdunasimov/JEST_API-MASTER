const request = require("../utils/axios");
const updatecat = (category_id, data, client_id, access_token) =>
  request(
    { 
    method: "put",
    url: `/v1/category/`+category_id,
    data,
    headers: {"client-id": client_id,
    "Authorization": access_token}, 
  });
  module.exports = { updatecat};