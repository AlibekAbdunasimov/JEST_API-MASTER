const request = require("../utils/axios");
const delopr = (operator_id,client_id, access_token) =>
  request(
    { 
    method: "delete",
    url: `/v1/operator/`+ operator_id,
    headers: {"client-id": client_id,
    "Authorization": access_token},
  });
  module.exports = { delopr};