const axios = require("axios");

const request = axios.create({
  baseURL: "https://api-manager.oxbox.uz",
  timeout: 8000,
});

const errorHandler = (error, hooks) => {
  if (error.response) {
    const data = error.response.data;
  }
  return Promise.reject(error.response);
};

// request interceptor
request.interceptors.request.use((config) => {
  //   const token = localStorage.getItem('token')
  //   config.headers['client-id'] = process.env.CLIENT_ID
  //   if (token) {
  //     config.headers['Authorization'] = token
  //   }
  return config;
}, errorHandler);

// response interceptor
request.interceptors.response.use((response) => {
  return response;
}, errorHandler);

module.exports = request;
