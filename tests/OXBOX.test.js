const shipperUser = require("../services/shipper-user");
const refreshtoken = require("../services/refreshtoken");
const logout = require("../services/logout");
const cats = require("../services/categories");
const createcat = require("../services/createcat");
const getcat = require("../services/getcat");
const updatecat = require("../services/updatecat");
var str = require("../functions/RandomString");
const delcat = require("../services/delcat");
const BindProject = require("../services/BindProject");
const getallops = require("../services/getallops");
const createopr = require("../services/createOPR");
var getopr = require("../services/getOPR");
var updateopr = require("../services/updateOPR");
let delopr = require('../services/delOPR');



let AUTH_TOKEN, CLIENT_ID = "4aad4651-146f-401a-b35d-20568caec65b", REFRESH_TOKEN, TOKEN_ID, Category_id, Operator_id;

function MainTest() {
  describe("Login admin user", () => {


    test("should successfully login ", async () => {
       let res = await shipperUser.loginShipper({
        login: "admin",
        password: "123456"
      }, 
      CLIENT_ID
      );
      REFRESH_TOKEN = res.data.token.refresh_token;
      AUTH_TOKEN = res.data.token.access_token;
      //console.log("Data:", res.data.token);
      //console.log("AUTH_TOKEN =", AUTH_TOKEN)
      expect(res.status).toEqual(200);
      console.log(1);
    });



    test("should not successfully login ", async () => {
      try {
        let res = await shipperUser.loginShipper({
           login: "admin",
           password: "wrong#paSS"
        },
        CLIENT_ID
        );
        //console.log("======", res2);
        expect(res.status).toEqual(400);
      } catch (e) {
        expect(e.status).toEqual(400);
        //console.log("Status:", e.status);
        console.log(2);
      }
    });



    test("Should open page by refresh token", async()=>{
      let res = await refreshtoken.refreshtoken({
        refresh_token: REFRESH_TOKEN,
      }, 
      CLIENT_ID
      );
      TOKEN_ID = res.data.token.id;
      //console.log("Status:", res.status);
      console.log(3);
    })



    test("Should logout from system", async()=>{
      let res = await logout.logout({
        token_id : TOKEN_ID,
      }, 
      CLIENT_ID
      );
      console.log(4);
      //console.log("Status:", res.status);
    })
});


  describe("Categories CRUD", ()=>{
    
    test("Should get all categories", async()=>
        {
            let res = await cats.getallcats(CLIENT_ID, AUTH_TOKEN);
            expect(res.status).toEqual(200);
            //console.log(Cat_ID);
            console.log(5);
        })


    test("Should create category", async()=>
    {
           let res = await createcat.createcat(
            {
              title: {
                kg: str.RandomSTR(10),
                kz: str.RandomSTR(10),
                ru: str.RandomSTR(10),
                tj: str.RandomSTR(10),
                uz: "Newly created"
              }
            }, CLIENT_ID, AUTH_TOKEN,)
            expect(res.status).toEqual(200);
            Category_id= res.data.response_data.id;
            //console.log(Category_id);
            console.log(6);
    })    


    test("Should get category", async()=>{

      let res = await getcat.getcat(CLIENT_ID, AUTH_TOKEN, Category_id)
      expect(res.status).toEqual(200);
      console.log(7);
    })


    test("Should not get category ", async () => {
      try {
        let res = await getcat.getcat(CLIENT_ID, AUTH_TOKEN, "g3y434-645yrth462-624")
        expect(res.status).toEqual(400);
      } catch (e) {
        expect(e.status).toEqual(400);
        //console.log("Status:", e.status);
        console.log(8);
      }
    })


    test("Should update category", async()=>
    {      
           let res = await updatecat.updatecat(Category_id,
            {
              title: {
                kg: str.RandomSTR(10),
                kz: str.RandomSTR(10),
                ru: str.RandomSTR(10),
                tj: str.RandomSTR(10),
                uz: "UPDATED"
              }
            }, CLIENT_ID, AUTH_TOKEN,)
            expect(res.status).toEqual(200);
            console.log(9);
    })
    
    
    test("Should delete category", async()=>
    {
           let res = await delcat.delcat(
            {
              categories: [
                Category_id
              ]
            }, CLIENT_ID, AUTH_TOKEN,)
            expect(res.status).toEqual(200);
            console.log(10);
    })  
    
    

    test("Should bind project to category", async()=>
    {
           let res = await BindProject.bindproject(
            {
              project_id: "0d5d075e-c2f4-4035-952c-1ad9eddfdc87"
            }, CLIENT_ID, AUTH_TOKEN,)
            expect(res.status).toEqual(200);
            console.log(11);
    }) 




      });
      describe("Operators CRUD", ()=>
      {

        test("Should get all operators", async()=>
        {
          let res = await getallops.getallops(CLIENT_ID, AUTH_TOKEN, "3003efa8-3050-4c3a-9b13-d3ddbccc8fc3")
          expect(res.status).toEqual(200);
          console.log(12);
        })


        test("Should create operator", async()=>
        {
          let res = await createopr.createopr(
            {
                "active": true,
                "firstname": str.RandomSTR(10),
                "image": {
                  "url": "string",
                  "uuid": "string"
                },
                "is_operator": true,
                "lastname": str.RandomSTR(10),
                "login": str.RandomSTR(10),
                "password": str.RandomSTR(10),
                "username": str.RandomSTR(10)
              }, CLIENT_ID, AUTH_TOKEN
            )
          expect(res.status).toEqual(200);
          Operator_id=res.data.id;
         console.log(13);
          
        })



        test("Should not create operator", async()=>
        { try{
          let res = await createopr.createopr(
            {
                "active": true,
                "firstname": str.RandomSTR(10),
                "image": {
                  "url": "string",
                  "uuid": "string"
                },
                "is_operator": true,
                "lastname": str.RandomSTR(10),
                "login": "Alibek0608",
                "password": str.RandomSTR(10),
                "username": str.RandomSTR(10)
              }, CLIENT_ID, AUTH_TOKEN
            )
          expect(res.status).toEqual(400);
          console.log(res.status);}
          catch (e) {
            expect(e.status).toEqual(400);
            //console.log("Status:", e.status);
            console.log(14);
          }
          
        })



        test("Should get operator", async()=>
        {
          let res = await getopr.getopr(Operator_id,CLIENT_ID, AUTH_TOKEN)
          expect(res.status).toEqual(200);
          console.log(15);
        })


        test("Should update operator", async()=>
        {
          let res = await updateopr.updateopr(Operator_id,CLIENT_ID, AUTH_TOKEN,
          {
            "active": true,
  "firstname": str.RandomSTR(11),
  "image": {
    "url": "string",
    "uuid": "string"
  },
  "is_operator": true,
  "lastname": str.RandomSTR(11),
  "login": "Alibek0608",
  "password": str.RandomSTR(11),
  "userName": str.RandomSTR(11)
          }
          )
          expect(res.status).toEqual(200);
          console.log(16);
        })



  //       test("Should not update operator", async()=>
  //       {
  //         try{
  //         let res = await updateopr.updateopr(Operator_id,CLIENT_ID, AUTH_TOKEN,
  //         {
  //           "active": true,
  // "firstname": str.RandomSTR(11),
  // "image": {
  //   "url": "string",
  //   "uuid": "string"
  // },
  // "is_operator": true,
  // "lastname": str.RandomSTR(11),
  // "login": "Alibek0608",
  // "password": str.RandomSTR(11),
  // "userName": str.RandomSTR(11)
  //         }
  //         )
  //         expect(res.status).toEqual(400);}
  //         catch (e) {
        //     expect(e.status).toEqual(400);
        //     //console.log("Status:", e.status);
        //   }
        // })


        test("Should delete operator", async()=>
        {
          let res = await delopr.delopr(Operator_id,CLIENT_ID, AUTH_TOKEN)
          expect(res.status).toEqual(204);
          console.log(17);
        })



        test("Should not delete operator", async()=>
        { try{
          let res = await delopr.delopr("Wrong-OPerAtor-id",CLIENT_ID, AUTH_TOKEN)
          expect(res.status).toEqual(400);}
          catch(e){expect(e.status).toEqual(400)}
          console.log(18);
        })

      })
}

// MAIN PLAYZONE
describe("OXBOX TEST", () => {
  MainTest();
});

module.exports = { MainTest };
